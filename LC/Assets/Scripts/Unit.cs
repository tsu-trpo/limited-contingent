﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Position {
    public int x;
    public int y;
    
    public Position (int _x, int _y) {
        x = _x;
        y = _y;
    }
}

public class Unit : MonoBehaviour {
    
    public enum Side { terrorists, USSR, neutral }
    
    private Side _side = Side.neutral;
    private int _number = 30;
    private float _combatSkill = 3;
    private float _firePower = 1;
    private float _previousStatic = 1;
    private float _currentStatic = 1;
    private float _morale = 3;
    private float _fitness = 4;
    private float _armor = 0;
    private Unit _atackTarget;
    private Position _position;
    
    
    public Side side { get { return _side; } }
    public int number { get { return _number; } }
    public float combatSkill { get { return _combatSkill; } }
    public float firePower { get { return _firePower; } }
    public float previousStatic { get { return _previousStatic; } }
    public float currentStatic { get { return _currentStatic; } }
    public float morale { get { return _morale; } }
    public float fitness { get { return _fitness; } }
    public float armor { get { return _armor; } }
    public Unit atackTarget { get { return _atackTarget; } }
    public Position position { get { return _position; } }
    
    
    public Unit setSide(Side side)
    {
        var rend = gameObject.GetComponent<Renderer>();
        rend.enabled = true;
        rend.sharedMaterial = Consts.sideColors[side];

        this._side = side;
        return this;
    }
    
    public Unit setNumber(int number)
    { this._number = number; return this; }
    
    public Unit setCombatSkill(float combatSkill)
    { this._combatSkill = combatSkill; return this; }
    
    public Unit setFirePower(float firePower)
    { this._firePower = firePower; return this; }
    
    public Unit setPreviousStatic(float previousStatic)
    { this._previousStatic = previousStatic; return this; }
    
    public Unit setCurrentStatic(float currentStatic)
    { this._currentStatic = currentStatic; return this; }
    
    public Unit setMorale(float morale)
    { this._morale = morale; return this; }
    
    public Unit setFitness(float fitness)
    { this._fitness = fitness; return this; }
    
    public Unit setArmor(float armor)
    { this._armor = armor; return this; }
    
    public Unit setAtackTarget(Unit unit)
    { this._atackTarget = unit; return this;}
    
    public Unit setPosition(Position position) {
        this._position = position;
        float xPosition = Coordinates.coordinatesMatrix[_position.y][_position.x].x;
        float zPosition = Coordinates.coordinatesMatrix[_position.y][_position.x].y;
        transform.localPosition = new Vector3(xPosition, transform.localPosition.z, zPosition);
        return this;
    }
    
    public Unit setPosition(int x, int y) {
        setPosition(new Position(x, y));
        return this;
    }
    
    public void copyFrom(Unit other) {
        this._side = other.side;
        this._number = other.number;
        this._combatSkill = other.combatSkill;
        this._firePower = other.firePower;
        this._previousStatic = other.previousStatic;
        this._currentStatic = other.currentStatic;
        this._morale = other.morale;
        this._fitness = other.fitness;
        this._armor = other.armor;
        this._atackTarget = other.atackTarget;
        this._position = other.position;
    }
    
    public void Kill (int numberKilled) {
        _number -= numberKilled;
        if (number <= 0) {
            Destroy(gameObject);
        }
    }
}
