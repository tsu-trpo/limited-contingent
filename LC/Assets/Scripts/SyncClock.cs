﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


class SyncClock : MonoBehaviour
{
    public delegate void TickDelegate();
    static public TickDelegate OnTickFire;
    static public TickDelegate OnTickMove;

    public int turn = 0;
    public int Turn { get{ return turn; } }

    bool pause = false;

    public static void DummyMethod() {}
    
    public void StopStart()
    {
        pause = !pause;
    }

    IEnumerator Clock()
    {
        while (true)
        {
            turn += 1;
            OnTickFire();
            OnTickMove();
            yield return new WaitForSeconds(2);
            while (pause)
                yield return new WaitForSeconds(0.2f);
        }
    }

    void Awake()
    {
        OnTickFire = DummyMethod;
        OnTickMove = DummyMethod;

        StartCoroutine(Clock());
    }
}