﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PlayerUnitCollection {
    
    private static List<Unit> collection = new List<Unit>();
    public static List<Unit> getCollection { get { return collection; } }
    
    public static void AddUnit (Unit unit) {
        collection.Add(unit);
    }
    
    public static void RemoveUnit (Unit unit) {
        collection.Remove(unit);
    }
}
