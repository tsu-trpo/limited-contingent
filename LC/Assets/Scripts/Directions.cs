﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Direction {
    NULL,
    NE,
    E,
    SE,
    SW,
    W,
    NW
}

public static class Directions {
    
    static private Dictionary<Direction, Vector2> shifts = new Dictionary<Direction, Vector2>();
    static private Dictionary<Direction, float> centralAnglesOfDirections = new Dictionary<Direction, float>();
    
    static Directions () {
        ShiftsInit();
    }
    
    static private void ShiftsInit () {
        Transform main = GameObject.Find("Main").transform;
        float xShift = main.localScale.x * Coordinates.DistanceBetweenHexes.x;
        float xHalfShift = xShift / 2f;
        float yShift = main.localScale.z * Coordinates.DistanceBetweenHexes.y;
        
        shifts.Add(Direction.NE, new Vector2(xHalfShift, yShift));
        shifts.Add(Direction.E, new Vector2(xShift, 0));
        shifts.Add(Direction.SE, new Vector2(xHalfShift, -yShift));
        shifts.Add(Direction.SW, new Vector2(-xHalfShift, -yShift));
        shifts.Add(Direction.W, new Vector2(-xShift, 0));
        shifts.Add(Direction.NW, new Vector2(-xHalfShift, yShift));
        shifts.Add(Direction.NULL, Vector2.zero);
    }
    
    static private void centralAnglesOfDirectionsInit () {
        centralAnglesOfDirections.Add(Direction.E, 0f);
        centralAnglesOfDirections.Add(Direction.NE, 60f);
        centralAnglesOfDirections.Add(Direction.NW, 120f);
        centralAnglesOfDirections.Add(Direction.W, 180f);
        centralAnglesOfDirections.Add(Direction.SW, 240f);
        centralAnglesOfDirections.Add(Direction.SE, 300f);
    }
    
    static public Direction ToDirection (float angle) {
        if (angle >= 30f && angle < 90f) {
            return Direction.NE;
        } else if (angle >= 90f && angle < 150f) {
            return Direction.NW;
        } else if (angle >= 150f && angle < 210f) {
            return Direction.W;
        } else if (angle >= 210f && angle < 270f) {
            return Direction.SW;
        } else if (angle >= 270f && angle < 330f) {
            return Direction.SE;
        } else
            return Direction.E;
    }
    
    static public Vector2 GetShift (Direction direction) {
        Vector2 shift;
        shifts.TryGetValue(direction, out shift);
        return shift;
    }
    
    static public float GetCentralAngleOfDirection (Direction direction) {
        float angle;
        centralAnglesOfDirections.TryGetValue(direction, out angle);
        return angle;
    }
    
    static public Position CalcPosition(Direction direction, Position currPosition) {
        Position newPosition = currPosition;
        int parityModificator = currPosition.y % 2;
        switch (direction) {
            case Direction.NE:
                newPosition.x += parityModificator;
                newPosition.y -= 1;
                break;
            case Direction.E:
                newPosition.x += 1;
                break;
            case Direction.SE:
                newPosition.x += parityModificator;
                newPosition.y += 1;
                break;
            case Direction.SW:
                newPosition.x -= (1 - parityModificator);
                newPosition.y += 1;
                break;
            case Direction.W:
                newPosition.x -= 1;
                break;
            case Direction.NW:
                newPosition.x -= (1 - parityModificator);
                newPosition.y -= 1;
                break;
            default:
                break;
        }
        return newPosition;
    }
}