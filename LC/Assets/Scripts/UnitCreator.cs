﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System;

public class UnitCreator : MonoBehaviour
{

    public GameObject Main;
    public Unit UnitPrefab;
    public Canvas UnitCanvasPrefab;
    public Text UnitLabelPrefab;

    void Awake()
    {
        Consts consts = this.GetComponent<Consts>();
        Consts.SideColorsInitialize(consts.sideMaterials);
        SetUnitsByConfig();
    }

    void SetUnitsByConfig()
    {
        string filePath = @"units.txt";
        string[] units = File.ReadAllLines(filePath);

        foreach (string unitConfBuffer in units) //int i = 0; i < units.Length; i += 1
        {
            string[] unitConf = unitConfBuffer.Split(',');
            SetUnit(unitConf);
        }
    }

    void SetUnit(string[] unitConf)
    {
        Unit unit = Instantiate<Unit>(UnitPrefab);

        unit.setSide((Unit.Side)Enum.Parse(typeof(Unit.Side), unitConf[0]));
        unit.setNumber(Int32.Parse(unitConf[1]));
        unit.setPosition(Int32.Parse(unitConf[2]), Int32.Parse(unitConf[3]));
        unit.setCombatSkill(float.Parse(unitConf[4]));
        unit.setFirePower(float.Parse(unitConf[5]));
        unit.setArmor(float.Parse(unitConf[6]));


        SetInGameSceneHierarchy(ref unit);
        if (unit.side == Unit.Side.USSR)
        {
            unit.gameObject.AddComponent<PathWay>();
            unit.gameObject.AddComponent<Moving>();
            PlayerUnitCollection.AddUnit(unit);
        }
        if (unit.side == Unit.Side.terrorists)
        {
            unit.gameObject.AddComponent<TerroristTargetController>();
        }

    }
    void SetInGameSceneHierarchy(ref Unit unit)
    {
        unit.transform.SetParent(Main.transform, false);
    }
}
