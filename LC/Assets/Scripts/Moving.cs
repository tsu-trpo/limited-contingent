﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moving : MonoBehaviour {
    
    private Unit unit;
    private PathWay pathWay;
    
    void Start () {
        unit = GetComponent<Unit>();
        pathWay = GetComponent<PathWay>();
        SyncClock.OnTickMove += Move;
    }
    
    public void Move () {
        Direction direction = pathWay.GetDirection();
        if (direction == Direction.NULL) {
            return;
        }
        Position newPosition = Directions.CalcPosition(direction, unit.position);
        unit.setPosition(newPosition);
    }
    
    private void OnDestroy () {
        SyncClock.OnTickMove -= Move;
    }
}
