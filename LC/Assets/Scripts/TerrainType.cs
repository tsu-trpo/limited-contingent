﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TerType { clearField, bouldersAndBushes, rareGrove }

static public class TerrainType
{
    static private Dictionary<TerType, float> terTypeModificator = new Dictionary<TerType, float>();
    static public Dictionary<TerType, float> TerTypeModificator { get{ return terTypeModificator; } }

    static TerrainType()
    {
        terTypeModificator.Add(TerType.clearField, 0.2f);
        terTypeModificator.Add(TerType.bouldersAndBushes, 0.5f);
        terTypeModificator.Add(TerType.rareGrove, 1.0f);
    }
}
