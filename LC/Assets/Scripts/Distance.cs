﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Distance {
    
    public static int DistanseInCubeCoordinates (Vector3 a, Vector3 b) {
        return (int)((Mathf.Abs(a.x - b.x) + Mathf.Abs(a.y - b.y) + Mathf.Abs(a.z - b.z)) / 2);
    }
    
    public static Vector3 TransformDecartToCubeCoordinates (Position decartCoordinates) {
        Vector3 cubeCoordinates;
        cubeCoordinates.x = decartCoordinates.x - (decartCoordinates.y - decartCoordinates.y % 2) / 2;
        cubeCoordinates.z = decartCoordinates.y;
        cubeCoordinates.y = -(cubeCoordinates.x + cubeCoordinates.z);
        return cubeCoordinates;
    }
}
