﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct HexSpec
{
    public int height;
    public TerType terType;

    public HexSpec(int h, TerType t)
    {
        height = h;
        terType = t;
    }
}

//____________________________________________//

static public class TerrainData
{

    static private List<List<HexSpec>> terrain = new List<List<HexSpec>>();
    static public List<List<HexSpec>> terrainMatrix { get { return terrain; } }


    static TerrainData()
    {
        int width = Coordinates.width;
        int height = Coordinates.height;


        for (int j = 0; j < height; j++)
        {
            terrain.Add(new List<HexSpec>());
            for (int i = 0; i < width; i++)
            {
                terrain[j].Add(new HexSpec(0, TerType.bouldersAndBushes)); ;
            }
        }
    }
}
