﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Coordinates
{ 
    static private List<List<Vector2>> coordinates =  new List<List<Vector2>>();
    static public List<List<Vector2>> coordinatesMatrix { get { return coordinates; } }

    static private Vector2 distanceBetweenHexes;
    static public Vector2 DistanceBetweenHexes { get { return distanceBetweenHexes; } }

    static private float horizontalDistBetweenCenterAndEdge;
    static private float verticalDistBetweenCenterAndVertex;

    static public int width = 10;
    static public int height = 10;

    static Coordinates()
    {
        InitDistanceBetweenHexes();
        MarkUp();
    }

    static private void MarkUp()
    {
        Vector2 origin = new Vector2(0.0f, 0.0f);

        origin = Consts.RectangLeftUpCorner + new Vector2(-horizontalDistBetweenCenterAndEdge, verticalDistBetweenCenterAndVertex);

        float xShift = 0;

        for (int j = 0; j < height; j++)
        {
            xShift = ((j + 1) % 2 == 0) ? -horizontalDistBetweenCenterAndEdge : 0;

            coordinates.Add(new List<Vector2>());
            for (int i = 0; i < width; i++)
            {
                float x = origin.x - (i * distanceBetweenHexes.x) + xShift;
                float y = origin.y + (j * distanceBetweenHexes.y);
                coordinates[j].Add(new Vector2(x, y));
            }
        }
    }

    static private void InitDistanceBetweenHexes()
    {
        horizontalDistBetweenCenterAndEdge = 1 / (1.0f + (2.0f * (float)width));
        verticalDistBetweenCenterAndVertex = 1 / (0.5f + (1.5f * (float)height));
        distanceBetweenHexes = new Vector2(horizontalDistBetweenCenterAndEdge * 2.0f, verticalDistBetweenCenterAndVertex * 1.5f); 
    }


}
