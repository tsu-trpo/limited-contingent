﻿using UnityEngine;

public class Targeting : MonoBehaviour {

    private Unit unit;
    private static Unit choosenUnit;

    void Start () {
        unit = GetComponent<Unit>();
        choosenUnit = null;
    }

    void OnMouseDown () {
        if (isPlayerUnit(unit)) {
            choosenUnit = unit;
            return;
        } else if (choosenUnit != null) {
            if (choosenUnit.atackTarget == unit)
                choosenUnit.setAtackTarget(null);
            else
                choosenUnit.setAtackTarget(unit);
            choosenUnit = null;
        }
    }
    
    private bool isPlayerUnit (Unit unit) {
        return (unit.side == Unit.Side.USSR);
    }
}
