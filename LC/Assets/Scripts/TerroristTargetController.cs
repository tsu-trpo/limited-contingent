﻿using System.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerroristTargetController : MonoBehaviour {
    
    private Unit unit;
    
    void Start () {
        unit = GetComponent<Unit>();
        StartCoroutine(Clock());
    }
    
    IEnumerator Clock () {
        while (true) {
            SetNewTerroristTarget(unit);
            yield return new WaitForSeconds(2);
        }
    }
    
    public static void SetNewTerroristTarget (Unit unit) {
        Vector3 unitPosition = Distance.TransformDecartToCubeCoordinates(unit.position);
        Func<Unit, int> selector = (a => Distance.DistanseInCubeCoordinates(Distance.TransformDecartToCubeCoordinates(a.position), unitPosition));
        Unit newTarget = PlayerUnitCollection.getCollection.OrderByDescending(selector).Last();
        unit.setAtackTarget(newTarget);
    }
}
