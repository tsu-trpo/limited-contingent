﻿using System.Collections;
using System.Collections.Generic;﻿
using UnityEngine;

public class CombatMath : MonoBehaviour {
    
    private Unit unit;
    
    void Start () {
        unit = GetComponent<Unit>();
        SyncClock.OnTickFire += DamageCount;
    }
    
    public void DamageCount ()
    {
        if (unit.atackTarget == null) {
            return;
        }
        
        float damage = DistanceModificator() * 2f * unit.combatSkill * unit.firePower * unit.number / (unit.previousStatic + unit.currentStatic);
        int numberKilled = Mathf.FloorToInt(damage / (6f * TerrainModificator() * unit.atackTarget.combatSkill + unit.atackTarget.armor));
        unit.atackTarget.Kill(numberKilled);
    }
    
    private float DistanceModificator () {
        Vector3 attackerCubeCoordinates = Distance.TransformDecartToCubeCoordinates(unit.position);
        Vector3 targetCubeCoordinates = Distance.TransformDecartToCubeCoordinates(unit.atackTarget.position);
        int distance = Distance.DistanseInCubeCoordinates(attackerCubeCoordinates, targetCubeCoordinates);
        float modificator = 2 * Mathf.Exp(-0.4f * (distance - 1));
        return modificator;
    }
    
    private float TerrainModificator () {
        float modificator;
        TerType terType = TerrainData.terrainMatrix[unit.atackTarget.position.x][unit.atackTarget.position.y].terType;
        TerrainType.TerTypeModificator.TryGetValue(terType, out modificator);
        return modificator;
    }
    
    private void OnDestroy() {
        SyncClock.OnTickFire -= DamageCount;
    }
}