﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Consts : MonoBehaviour {

    public static readonly Vector2 RectangLeftUpCorner = new Vector2(0.5f, -0.5f);
    public Material[] sideMaterials;

    public static Dictionary<Unit.Side, Material> sideColors = new Dictionary<Unit.Side, Material>();

    public static void SideColorsInitialize(Material[] sideMaterials)
    {
        sideColors[Unit.Side.neutral] = sideMaterials[0];
        sideColors[Unit.Side.USSR] = sideMaterials[1];
        sideColors[Unit.Side.terrorists] = sideMaterials[2];
    }
}
