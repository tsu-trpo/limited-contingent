﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class PathWay : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler {
    
    private enum State {
        Idle,
        Drawing
    }
    private Vector3 centerPosition;
    private List<Direction> way = new List<Direction>();
    private int index = 0;
    private State state;
    private static int rightMouseButtonId = -2;
    private static float minDistanceForChangeCenter;
    
    void Start () {
        minDistanceForChangeCenter = Coordinates.DistanceBetweenHexes.y * transform.parent.transform.localScale.x / 1.5f;
        state = State.Idle;
    }
    
    public void OnDrag (PointerEventData data) {
        if (data.pointerId != rightMouseButtonId) {
            return;
        }
        Vector3 mousePosition = data.pointerCurrentRaycast.worldPosition;
        mousePosition.z = 0;
        Vector3 direction = Vector3.Normalize(mousePosition - centerPosition);
        float distance = Vector3.Distance(mousePosition, centerPosition);
        if (distance > minDistanceForChangeCenter) {
            float angle = Vector3.Angle(direction, Vector3.right);
            float ortoAngle = Vector3.Angle(direction, Vector3.up);
            way.Add(Directions.ToDirection(CalcAngle(angle,ortoAngle)));
            SetNewCenter(way.Last());
        }
    }
    
    public void OnPointerDown (PointerEventData data) {
        if (data.pointerId != rightMouseButtonId) {
            return;
        }
        way.Clear();
        index = 0;
        centerPosition = transform.position;
        centerPosition.z = 0f;
        state = State.Drawing;
    }
    
    public void OnPointerUp(PointerEventData data) {
        if (data.pointerId == rightMouseButtonId) {
            state = State.Idle;
        }
    }
    
    private void SetNewCenter (Direction direction) {
        centerPosition.x += Directions.GetShift(direction).x;
        centerPosition.y += Directions.GetShift(direction).y;
    }
    
    private float CalcAngle (float xAngle, float yAngle) {
        float angle = 0f;
        if (xAngle < 90f && yAngle <= 90f) {
            angle = xAngle;
        }  else if (xAngle >= 90f && yAngle < 90f) {
            angle = 90f + yAngle;
        } else if (xAngle > 90f && yAngle >= 90f) {
            angle = 90f + yAngle;
        } else if (xAngle <= 90f && yAngle > 90f) {
            angle = 360f - xAngle;
        }
        return angle;
    }
    
    public Direction GetDirection() {
        if (state == State.Drawing || index >= way.Count) {
            return Direction.NULL;
        }
        return way[index++];
    }
}